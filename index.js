import EmailValidator from 'email-validator'
import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat.js'
dayjs.extend(customParseFormat)

// const nameValidationPattern = /^[А-ЯЁ][А-ЯЁа-яё\s-]{0,30}$/
const nameValidationPattern = /^([А-ЯЁ]-[а-яё]|[А-ЯЁ]\s?)([а-яёА-ЯЁ]\s?[а-яёА-ЯЁ]?|[а-яёА-ЯЁ]-[а-яёА-ЯЁ]?)*[а-яёА-ЯЁ]\s*$/
// const optionalNameValidationPattern = /^[А-ЯЁа-яё\s-]*$/
const optionalNameValidationPattern = /^([А-ЯЁа-яё]\s?|[А-ЯЁа-яё]-[А-ЯЁа-яё]?)*[а-яёА-ЯЁ]\s*$/
const passwordPattern=/^(?=^.{8,}$)(?=.*\d)(?=.*[a-zA-Zа-яА-Я])(?!.*\s).*$/
const passwordMinLength=8
export const passwordMaxLength=63
/** Limit for first_name last_name data inputs */
export const maxNameLength = "31"
/** Limit for passport issued by input data */
export const maxIssuedByLength="255"
/** Sets limit for the company name input field */
export const maxPosNameLength = 255;

export const phoneIsValid = ( phone ) =>  phone.length === 10

export const passSeriesIsValid = (passSeries) => passSeries.length === 4
export const passNumberIsValid = (passNumber) => passNumber.length === 6
export const dateIsValid = (issuedDate) => issuedDate  ===
  dayjs( issuedDate.substr(0,10), "DD.MM.YYYY").format("DD.MM.YYYY")

export const snilsIsValid = (data) => {
  const numberPattern = /\d+/g;
  const digits = ( data.match( numberPattern ) || [] ).join('')
  if (digits.length < 11) return false;
  const snils = digits.substr(0,11)
  if (/[^0-9]/.test(snils)) return false;
  var sum = 0;
  for (var i = 0; i < 9; i++) {
    sum += parseInt(snils[i]) * (9 - i);
  }
  var checkDigit = 0;
  if (sum < 100) {
    checkDigit = sum;
  } else if (sum > 101) {
    checkDigit = parseInt(sum % 101);
    if (checkDigit === 100) {
      checkDigit = 0;
    }
  } else {
    checkDigit = 0
  }
  return checkDigit === parseInt(snils.slice(-2))
}

export const passwordIsValid = (password) => {
  return passwordPattern.test(password) &&
    password.length <= passwordMaxLength &&
    password.length >= passwordMinLength
}

export const nameIsValid = ( name ) => nameValidationPattern.test( name )
export const optionalNameIsValid = (name) =>
  optionalNameValidationPattern.test(name)

export const emailIsValid = (email) => EmailValidator.validate( email )

